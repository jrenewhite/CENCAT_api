<?php
     //Incluye las librerias de la API
     require_once("api_builder_includes/class.API.inc.php");

     //Indica que la salida ser� JSON
     header("Content-Type: application/json; charset=utf-8");
     include 'api_builder_includes/dbconnect.php';
     $mysql_table="tbl_usuarios_app";
      //Si algun parametro de la API se incluyo en el http request mediante $_GET...
      if(isset($_GET) && !empty($_GET)){

        //Especifica las columnas de salida mediante comas
        $columns = "ID, USERNAME, PASSWORD, ACTIVO, FECHA_INGRESO, ULTIMO_INGRESO, API_key, API_hits, API_hit_date";

        //Inicia la API
        $api = new API($mysql_host, $mysql_database, $mysql_table, $mysql_user, $mysql_password);

        $api->setup($columns);
        $api->set_default_order("ID");
        $api->set_default_search_order("USERNAME");
        //$api->set_searchable("USERNAME");
        $api->set_pretty_print(false);

        //Limpia los contenidos de $_GET para asegurarte que 
        //strings maliciosos no puedan corromper tu base de datos
        $get_array = Database::clean($_GET);

        //Regresa los resultados del http request
        echo $api->get_json_from_assoc($get_array);
    }
?>
