<?php

	 //include the API Builder mini lib
	 require_once("api_builder_includes/class.API.inc.php");

	 //set page to output JSON
	 header("Content-Type: application/json; charset=utf-8");
	 
	  //If API parameters were included in the http request via $_GET...
	  if(isset($_GET) && !empty($_GET)){

	  	//specify the columns that will be output by the api as a comma-delimited list
	  	$columns = "ID,DISTRITO,PROVINCIA";

	  	//setup the API
	  	$api = new API("localhost", 
	  				   "actualizacion_idaan", 
	  				   "cat_distrito", 
	  				   "root", 
	  				   "root");

	  	$api->setup($columns);
	  	$api->set_default_order("ID");
	  	$api->set_default_search_order("DISTRITO,PROVINCIA");
	  	$api->set_pretty_print(false);

	  	//sanitize the contents of $_GET to insure that 
	  	//malicious strings cannot disrupt your database
	 	$get_array = Database::clean($_GET);

	 	//output the results of the http request
	 	echo $api->get_json_from_assoc($get_array);
	}
?>