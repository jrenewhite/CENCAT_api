$(document).ready(function() {
	$('#picker').farbtastic('#color, body');
	$(document).on("click","#button-setting",function(){
		$(".setting").animate({
			left:($(".setting").css("left")=="0px")?"-210px":"0px",
		}, 500);
	});
	
	$(document).on("click",".sett-bg",function(){
		$('.sett-bg,.sett-bg-none').css({'border':'1px solid #ccc'});
		$("#bg-shadow").attr('src','assets/img/'+$(this).attr("title")).show();
		$(this).css({'border':'2px solid #000'})
	});
	$(document).on("click",".sett-bg-none",function(){
		$('.sett-bg').css({'border':'1px solid #ccc'});
		$("#bg-shadow").hide();
		$(this).css({'border':'2px solid #000'})
	});
	

	
	$("#logoShow").click(function(){
        if (this.checked){
            $(".logo").show();
        }
        else {
            $(".logo").hide();
        }
            return true;
		});
	});