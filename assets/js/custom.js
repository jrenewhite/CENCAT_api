	$(document).ready(function () {
		/* Style set */
		$('body').css('backgroundColor',setting.style.bodyColor);
		if(setting.style.bodyShadow){
			$('.bg-shadow').html($( '<img/>', {
				'src': 'assets/img/bg' +setting.style.bodyShadow+ '.png',
				'id':'bg-shadow',
				'alt':''
			}));
		}
		
		
		/* Counter */
		digitCount('#dig-days',
			'<span>{d100}</span>'+
			'<span>{d10}</span>'+
			'<span>{d1}</span>'
		);
		digitCount('#dig-hours','{hnn}');
		digitCount('#dig-mins','{mnn}');
		digitCount('#dig-sec','{snn}');
		digitCount('#count-small',
			'<span class="dark">{dnn}</span>:' +
			'<span class="light">{hnn}</span>:' +
			'<span class="dark">{mnn}</span>:' +
			'<span class="light">{snn}</span>'
		);
		 
		
		
		/* Language switcher */
		 var langs = getParameterValue('lang');
		(langs != "") ? langs : '';
		$('[data-localize]').localize('lang', {
			language: langs,
			pathPrefix: 'lang',
			skipLanguage: /^en/,
			callback: function (data, defaultCallback) {
				document.title = data.name.name_light + ' ' + data.name.name_dark;
				defaultCallback(data);
				window.lName = data.name.name_light;
			}
		});
		
	

		/* Validation and ajax post subscribe */
		 $("#subscribe-form").validate({
			errorElement: "small",
			onkeyup: false,
			rules: {
				"subscribe[mail]": {
					required: true,
					email: true
				}
			}, 
			messages: {
				"subscribe[mail]": {
					required: $('#tempRequired').text(),
					email: $('#tempMail').text()
				}
			},
			submitHandler: function (form) {
				$.ajax({
					success: function (data) {
						if (data == 0) {
							$('#success').html($('#tempSuccess').html())
								.show().delay(2000).fadeOut(500);
						} else if (data == 1) {
							$(form).validate().showErrors({
								'subscribe[mail]': $('#tempMail').text()
							});
						} else if (data == 2) {
							$(form).validate().showErrors({
								'subscribe[mail]': $('#tempSubs').text()
							});
						} else {
							$(form).validate().showErrors({
								'subscribe[mail]': 'Error request!'
							});
						}
					},
					statusCode: {
						404: function () {
							alert('Not found actions.php');
						}
					},
					type: 'POST',
					url: 'assets/php/actions.php',
					cache: false,
					data: $(form).serialize()
				});
			}
		}); 

	 	$('#button-lang').click(function () {
			$('#modal-lang').reveal();
			return false;
		}); 
		
		setInterval(function () {
			$('.dark').css('color',getContrast(setting.style.bodyColor));
		}, 1000);
		
	});
	
	/* Animation effects */
	$(window).load(function () {
		$('#circleG').remove();
		$('.p-p-container').switchClass( "p-p-container", "p-container", 1000,"swing", function(){
			$('.p-floater').switchClass( "p-floater", "floater", 1000);
			$('.p-container').switchClass( "p-container", "container", 1000,"swing", function(){
				$('.show1').delay(100).animate({opacity: '1'});
				$('.show2').delay(500).animate({opacity: '0.8'});
				$('.show3').delay(600).animate({opacity: '1'});
				$('.show4').delay(700).animate({opacity: '1'});
			});
		});
	});
	
 	function digitCount(select,layout){
		$(select).countdown({
			until: new Date(Date.parse(setting.lastDate)),
			compact: true,
			layout:layout,
			timezone:setting.timeZone
		});

	}
	
	function getParameterValue(parameter) {
		parameter = parameter.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + parameter + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if (results == null) return "";
		else return results[1];
	}

 	function getContrast(hexcolor){
		var re = /^#((([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})){1,2})$/;
		if(re.test(hexcolor)){
			hexcolor = re.exec(hexcolor);
			if(hexcolor[1].length == 3){
				hex = hexcolor[3]+hexcolor[3]+hexcolor[4]+hexcolor[4]+hexcolor[5]+hexcolor[5];
			} else {
				hex = hexcolor[1];
			}
			
			var r = parseInt(hex.substr(0,2),16);
			var g = parseInt(hex.substr(2,2),16);
			var b = parseInt(hex.substr(4,2),16);
			var yiq = ((r*299)+(g*587)+(b*114))/1000;
			return (yiq > 60) ? 'black' : 'white';
		} else {
			alert('Color code is not yet valid');
		}
	}
	
	
	var bColor = getParameterValue('color');
	if (bColor != "")
		setting.style.bodyColor = (bColor != "") ? '#'+bColor : null;
	var bShadow = getParameterValue('shadow');
	if(bShadow != "")
		setting.style.bodyShadow = (bShadow != "") ? bShadow : null;
